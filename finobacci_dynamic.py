import timeit


def finobacci(n):
    f0 = 0
    f1 = 1
    f2 = 1
    for i in range(2, n + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


def re_fino(n):
    if n <= 1:
        return n
    else:
        return re_fino(n - 1) + re_fino(n - 2)


if __name__ == "__main__":
    print(
        "This is finobacci with bottom to up :",
        finobacci(10),
        "\n" "This is time of finobacci with bottom to up :",
        timeit.timeit("finobacci(10)", globals=globals()),
    )
    print(
        "This is finobacci with recursive :",
        re_fino(10),
        "\n" "This is time of finobacci with recursive :",
        timeit.timeit("re_fino(10)", globals=globals()),
    )
